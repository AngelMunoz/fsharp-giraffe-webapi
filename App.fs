namespace GiraffeHeroes

open FSharp.Control.Tasks.V2.ContextInsensitive
open Microsoft.AspNetCore.Http
open Giraffe
open Types

module App =
    [<RequireQualifiedAccess>]
    module HeroHandlers =
        let get: HttpHandler =
            fun (next: HttpFunc) (ctx: HttpContext) ->
                task { return! Successful.OK (Hero.find ()) next ctx }

        let findOne (id: int): HttpHandler =
            fun (next: HttpFunc) (ctx: HttpContext) ->
                task {
                    match Hero.findOne id with
                    | Some hero -> return! Successful.OK hero next ctx
                    | None -> return! RequestErrors.NOT_FOUND ({| message = "Not Found" |}) next ctx
                }

        let create: HttpHandler =
            fun (next: HttpFunc) (ctx: HttpContext) ->
                task {
                    let! payload = ctx.BindJsonAsync<HeroPayload>()
                    ctx.SetStatusCode(201)
                    return! Successful.OK (Hero.create payload.name payload.powers) next ctx
                }

        let update (id: int): HttpHandler =
            fun (next: HttpFunc) (ctx: HttpContext) ->
                task {
                    let! hero = ctx.BindJsonAsync<Hero>()
                    return! Successful.OK (Hero.update id hero) next ctx
                }

        let delete (id: int): HttpHandler =
            fun (next: HttpFunc) (ctx: HttpContext) ->
                task { return! Successful.OK (Hero.delete id) next ctx }

    let webApp: HttpHandler =
        choose [ GET
                 >=> choose [ route "/heroes" >=> HeroHandlers.get
                              routef "/heroes/%i" HeroHandlers.findOne ]
                 POST >=> route "/heroes" >=> HeroHandlers.create
                 PUT >=> routef "/heroes/%i" HeroHandlers.update
                 DELETE >=> routef "/heroes/%i" HeroHandlers.delete
                 setStatusCode 404 >=> text "Not Found" ]
