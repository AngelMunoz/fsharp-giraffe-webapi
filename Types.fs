namespace GiraffeHeroes

module Types =
    type Hero =
        { Id: int
          Name: string
          Powers: seq<string> }

    type HeroPayload = { name: string; powers: seq<string> }


[<RequireQualifiedAccess>]
module Hero =
    open Types

    let mutable private db: list<Hero> = []

    let create (name: string) (powers: seq<string>) =
        db <-
            { Id = db.Length + 1
              Name = name
              Powers = powers |> Seq.toList }
            :: db
        db.[db.Length - 1]

    let find (): list<Hero> = db

    let findOne (id: int): Option<Hero> = db |> List.tryFind (fun h -> h.Id = id)


    let update (id: int) (hero: Hero): Hero =
        db <-
            db
            |> List.map (fun h -> if h.Id = id then hero else h)
        hero

    let delete (id: int): int =
        db <- db |> List.filter (fun h -> h.Id <> id)
        id
